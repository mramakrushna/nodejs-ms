# Seneca Sample Illustrating
### 1)   Module exporting a Service with patterns  math.js
### 2)   Exposing the Service Patterns defined above to outside world with http using math-tree.js
### 3)   Consuming the Service Patterns from a client app micro_serv_clnt.js


## Pre-requisites
    ###  1)  Node JS Installation version 7.0+ (installs node and npm )
    ###  2)  Seneca Installation  
              $ npm install seneca --g
    
    
## Running the above Service 
        $ node math-tree.js
        $ 
        
        
## Running the Client        
        $  node micro_serv_clnt.js
        


