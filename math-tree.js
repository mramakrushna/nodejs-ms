// run command:

// $ node math-tree.js --seneca.print.tree



require('seneca')()
  .use('math')
  .listen({ port: 3000, host: 'localhost' })

// // seneca.client({ port: 8080, host: '192.168.0.2' }) → seneca.listen({ port: 8080, host: '192.168.0.2' })
// from browser access
//  http://localhost:10101/act?role=math&cmd=sum&left=1&right=2
// from console use curl tool
// curl -d '{"role":"math","cmd":"sum","left":1,"right":2}' http://localhost:10101/act
